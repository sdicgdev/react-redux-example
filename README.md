Code resulting from the tutorial at http://teropa.info/blog/2015/09/10/full-stack-redux-tutorial.html

to start:

    $ npm install 

    $ npm start

    $ cd voting-client

    $ npm install
    
    $ npm start

to see the app:

http://localhost:8080/webpack-dev-server/

http://localhost:8080/webpack-dev-server/#/results

you can use your computer's local ip address to see the app from another device